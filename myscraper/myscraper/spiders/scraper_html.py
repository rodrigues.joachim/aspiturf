# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import os
import re
import sys
from imp import reload

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Request, Rule

from myscraper.items import MyscraperItem

reload(sys)
sys.setdefaultencoding('utf-8')

# Va chercher les pages html
class PTDirectorySpider(CrawlSpider):
    name = 'scraper_html'
    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 12,
        'RETRY_TIMES': 0,
        'PROXY': 'http://127.0.0.1:8888/?noconnect',
        'API_SCRAPOXY': 'http://127.0.0.1:8889/api',
        'API_SCRAPOXY_PASSWORD': 'souris',
        'HTTPCACHE_ENABLED': False,
        'HTTPCACHE_EXPIRATION_SECS': 0,
        'HTTPCACHE_DIR': 'httpcache',
        'HTTPCACHE_IGNORE_HTTP_CODES': [404,500,407,504],
        'HTTPCACHE_STORAGE': 'scrapy.extensions.httpcache.FilesystemCacheStorage',
        'ITEM_PIPELINES': {
            'myscraper.pipelines.HtmlFilePipeline': 400
        },
        'DOWNLOADER_MIDDLEWARES': {
            'scrapoxy.downloadmiddlewares.proxy.ProxyMiddleware': 100,
            'scrapoxy.downloadmiddlewares.wait.WaitMiddleware': 101,
            'scrapoxy.downloadmiddlewares.scale.ScaleMiddleware': 102,
            'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
            'scrapoxy.downloadmiddlewares.blacklist.BlacklistDownloaderMiddleware': 950,
        }
    }
    allowed_domains = ['www.paris-turf.com']

    def start_requests(self):
        try:
            self.tagdate
        except:
            self.tagdate = datetime.datetime.now().date()
            self.tagdate = str(self.tagdate)
        try:
            self.i
        except:
            self.i = 1

        self.date = datetime.datetime.strptime(self.tagdate, '%Y-%m-%d')
        for x in xrange(1, int(self.i) + 1):
            yield scrapy.Request('http://www.paris-turf.com/menu/courses/' + self.date.strftime('%Y-%m-%d'), self.parse)
            self.date += datetime.timedelta(days=1)

    def parse(self, response):
        urls = response.xpath(u'//li/a/@href').extract()

        for url in urls:
            yield Request(
                url=response.urljoin(url),
                callback=self.parse_race,
            )

    def parse_race(self, response):
        item = MyscraperItem()
        jour = re.findall(r"\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/", response.url, re.UNICODE)
        item['jour'] = ''.join(jour)
        item['jour'] = item['jour'].replace('/', '')
        item['url'] = response.url
        item['html'] = response.body
        return item
