# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import errno
import io
import json
import os
import os.path
import re
import sys
from imp import reload

import pytz
import requests
import scrapy
from scrapy.spiders import CrawlSpider, Request

from myscraper.items import MyscraperItem

#cachedate

reload(sys)
sys.setdefaultencoding('utf-8')

class PTDirectorySpider(CrawlSpider):
    name = 'scraper_rapports'
    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 10,
        'CONCURRENT_REQUESTS': 10,
        'RETRY_TIMES': 0,
        # 'PROXY': 'http://127.0.0.1:8888/?noconnect',
        # 'API_SCRAPOXY': 'http://127.0.0.1:8889/api',
        # 'API_SCRAPOXY_PASSWORD': 'souris',
        # 'HTTPCACHE_ENABLED': False,
        # 'HTTPCACHE_EXPIRATION_SECS': 0,
        # 'HTTPCACHE_DIR': 'httpcache',
        # 'HTTPCACHE_IGNORE_HTTP_CODES': [404,500,407,504],
        # 'HTTPCACHE_STORAGE': 'scrapy.extensions.httpcache.FilesystemCacheStorage',
        'ITEM_PIPELINES': {
            'myscraper.pipelines.MyscraperRapports': 300,
            # 'myscraper.pipelines.MyscraperCaractRap': 500
        },


        # 'CONCURRENT_REQUESTS_PER_DOMAIN': 12,
        # 'RETRY_TIMES': 0,
        # 'PROXY': 'http://127.0.0.1:8888/?noconnect',
        # # 'API_SCRAPOXY': 'http://127.0.0.1:8889/api',
        # # 'API_SCRAPOXY_PASSWORD': 'souris',
        # 'HTTPCACHE_ENABLED': False,
        # 'HTTPCACHE_EXPIRATION_SECS': 0,
        # 'HTTPCACHE_DIR': 'httpcache',
        # 'HTTPCACHE_IGNORE_HTTP_CODES': [404,500,407,504],
        # 'HTTPCACHE_STORAGE': 'scrapy.extensions.httpcache.FilesystemCacheStorage',
        # 'ITEM_PIPELINES': {
        #     'myscraper.pipelines.MyscraperRapports': 400,
        # },


        # 'DOWNLOADER_MIDDLEWARES': {
        #     'scrapoxy.downloadmiddlewares.proxy.ProxyMiddleware': 100,
        #     'scrapoxy.downloadmiddlewares.wait.WaitMiddleware': 101,
        #     'scrapoxy.downloadmiddlewares.scale.ScaleMiddleware': 102,
        #     'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
        #     'scrapoxy.downloadmiddlewares.blacklist.BlacklistDownloaderMiddleware': 950,
        # }
    }

    def start_requests(self):
        try:
            self.tagdate
        except:
            self.tagdate = datetime.datetime.now().date()
            self.tagdate = str(self.tagdate)
        try:
            self.i
        except:
            self.i = 2

        self.logger.info('tagdate==========> %s', self.tagdate)
        self.date = datetime.datetime.strptime(self.tagdate, '%Y-%m-%d')
        self.datedmY = self.date.strftime('%d%m%Y')

        for x in xrange(1, int(self.i) + 1):
            print('self.i*-*-*-*-*-*===>', self.i)
            self.logger.info('self.i*-*-*-*-*-*===> %s', self.i)
            self.datedmY = self.date.strftime('%d%m%Y')
            self.dateYmd = self.date.strftime('%Y-%m-%d')

            yield scrapy.Request(
                'http://127.0.0.1:8000/' + self.dateYmd,
                meta={
                    'jour': self.dateYmd,
                    'jourdmY': self.datedmY
                },
                callback=self.parse1
            )
            print('===========!!!!!!!!!!!', self.dateYmd)
            self.logger.info('dateYmd==========> %s', self.dateYmd)
            self.date += datetime.timedelta(days=1)

    # Cherche course Informations
    def parse1(self, response):
        print('====================', response.meta['jour'])
        self.logger.info('response.meta[jour] ==========> %s', response.meta['jour'])
        urls = response.xpath(u'//li/a/@href').extract()
        print('=========- URLs -===========', urls )
        for url in urls:
            # print('=========- URL -===========', url )
            self.logger.info('=========- URL -=========== %s', url)
            if (url[-4:] != 'json'):
                numcourse = re.findall('[0-9]{6,}', url)
                self.logger.info('numcourse=======> %s', numcourse)
                yield Request(
                    url=response.urljoin(url),
                    callback=self.parse_race1,
                    meta={
                        # 'prix': prixNum[i],
                        'jour': response.meta['jour'],
                        'numcourse': numcourse,
                        'url': url,
                        'jourdmY': response.meta['jourdmY']
                    }
                )

    def parse_race1(self, response):
        item = MyscraperItem()

        numcourse= response.meta['numcourse']
        rapports = []

        numcourse = int(''.join(numcourse))
        jour = response.meta['jour']
        numcourse = int(numcourse) * int(jour[0:4])

        word_list = ['Gagnant', 'ordre', 'Bonus', 'Tirelire']

        # Cherche pari simple
        tabHeader = response.xpath('//*[@class="table reports first"]')
        simple = tabHeader.xpath('.//thead/tr[2]/th[2]/img').extract()

        try:
            re.search('PMH.png', simple[0])
            tabSimple = tabHeader.xpath('.//tbody')

            for row in tabSimple.xpath('.//tr'):

                pari_type = row.xpath('.//td[1]/text()').get()
                rapport = row.xpath('.//td[2]/text()').get().strip()
                rap = [numcourse, 'simple', pari_type.split('>')[1].strip(), pari_type.split('>')[0].strip(), rapport.replace(',', '.'), jour]
                rapports.append(rap)
        except:
            pass

        for table in response.xpath('//*[@class="table reports "]'):
            typep = table.xpath('.//thead/tr[1]/th[2]/img/@alt').get()

            try:
                rapport_type = typep.replace('Paris PMU', '')

            except:
                rapport_type = ''

            if (rapport_type != ''):
                tabSimple = table.xpath('.//tbody')

                for row in tabSimple.xpath('.//tr'):

                    pari_type_text = row.xpath('.//td[1]/text()').get()
                    try:
                        pari_type = pari_type_text.split('>')[1].strip()
                        horses = pari_type_text.split('>')[0].strip()
                    except:
                        pari_type = 'other'
                        try:
                            horses = pari_type_text.strip()
                        except:
                            horses = 'null'
                    if (table.xpath('.//thead/tr[1]/th[1]/text()').get() != None):
                        horses = table.xpath('.//thead/tr[1]/th[1]/text()').get()

                    for value in word_list:
                        matching = re.findall(value, horses, re.UNICODE)
                        if matching != []:
                            pari_type = horses
                            horses = ''

                    try:
                        rapport = row.xpath('.//td[2]/text()').get().strip()
                    except:
                        pass
                    rap = [numcourse, rapport_type, pari_type, horses, rapport.replace(',', '.'), jour]
                    rapports.append(rap)

        item['th'] = ['caractrap_id', 'rapport_type', 'pari_type', 'horses', 'rapport', 'jour' ]
        item['alll'] = json.loads(json.dumps(rapports))
        return item
