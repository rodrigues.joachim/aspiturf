# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import errno
import io
import json
import os
import os.path
import re
import shutil
import sys
import time
from imp import reload
from pathlib import Path

import pytz
import requests
import scrapy
from bs4 import BeautifulSoup
from pytz import timezone
from scrapy.http.response.html import HtmlResponse
from scrapy.linkextractors import LinkExtractor
from scrapy.mail import MailSender
from scrapy.spiders import CrawlSpider, Request, Rule

from myscraper.items import MyscraperItem

#cachedate
mailer = MailSender(smtphost='localhost', mailfrom='mailfrom@gmail.com', smtpuser='', smtppass='', smtpport=1025)


reload(sys)
sys.setdefaultencoding('utf-8')

class PTDirectorySpider(CrawlSpider):
    name = 'scraper_partant'
    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 12,
        'RETRY_TIMES': 0,
        'PROXY': 'http://127.0.0.1:8888/?noconnect',
        'API_SCRAPOXY': 'http://127.0.0.1:8889/api',
        'API_SCRAPOXY_PASSWORD': 'souris',
        'HTTPCACHE_ENABLED': False,
        'HTTPCACHE_EXPIRATION_SECS': 0,
        'HTTPCACHE_DIR': 'httpcache',
        'HTTPCACHE_IGNORE_HTTP_CODES': [404,500,407,504],
        'HTTPCACHE_STORAGE': 'scrapy.extensions.httpcache.FilesystemCacheStorage',
        'ITEM_PIPELINES': {
            'myscraper.pipelines.MyscraperPartant': 400,
            'myscraper.pipelines.MyscraperCaractRap': 500
        },
        'DOWNLOADER_MIDDLEWARES': {
            'scrapoxy.downloadmiddlewares.proxy.ProxyMiddleware': 100,
            'scrapoxy.downloadmiddlewares.wait.WaitMiddleware': 101,
            'scrapoxy.downloadmiddlewares.scale.ScaleMiddleware': 102,
            'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
            'scrapoxy.downloadmiddlewares.blacklist.BlacklistDownloaderMiddleware': 950,
        }
    }

    def start_requests(self):
        try:
            self.tagdate
        except:
            self.tagdate = datetime.datetime.now().date()
            self.tagdate = str(self.tagdate)
        try:
            self.i
        except:
            self.i = 2

        self.logger.info('tagdate==========> %s', self.tagdate)
        self.date = datetime.datetime.strptime(self.tagdate, '%Y-%m-%d')
        self.datedmY = self.date.strftime('%d%m%Y')

        #Supprime dossier des fichiers json
        try:
            shutil.rmtree("files/" + self.date.strftime('%Y-%m-%d'))
        except :
            pass

        for x in xrange(1, int(self.i) + 1):
            yield scrapy.Request(
                'http://www.paris-turf.com/menu/courses/' + self.date.strftime('%Y-%m-%d'),
                meta={
                    'jour': self.date.strftime('%Y-%m-%d'),
                    'jourdmY': self.datedmY
                },
                callback=self.parse1)

            self.date += datetime.timedelta(days=1)
            self.datedmY = self.date.strftime('%d%m%Y')

    # Cherche course Informations
    def parse1(self, response):
        print('====================', response.meta['jour'])
        prix = response.xpath('//*[@class="num"]/text()').extract()
        prixNum = []
        for pri in prix:
            prixNum.append(pri)

        i = 0
        urls = response.xpath(u'//li/a/@href').extract()
        for url in urls:
            print('=========- URL -===========', response.urljoin(url), prixNum[i] )
            numcourse = re.search('[0-9]{6,}$', url)
            numcourse = numcourse.group()
            print('numcourse=======>', numcourse)
            yield Request(
                url=response.urljoin(url),
                callback=self.parse_race1,
                meta={
                    'prix': prixNum[i],
                    'jour': response.meta['jour'],
                    'numcourse': numcourse,
                    'url': url,
                    'jourdmY': response.meta['jourdmY']
                }
            )
            i += 1

    # scrape courses Information
    def parse_race1(self, response):
        item = MyscraperItem()
        item['url']= ''
        item['arriv']= ''
        item['prixnom']= ''
        item['hippo']= ''
        item['reun']= ''
        item['prix']= response.meta['prix']
        item['sex']= ''
        item['handi']= ''
        item['meteo']= ''
        item['tempscourse']= ''
        item['corde']= ''
        item['dist']= ''
        item['typec']= ''
        item['reclam']= ''
        item['courseabc']= ''
        item['autos']= ''
        item['cheque']= ''
        item['natpis']= ''
        item['condi']= ''
        item['pistegp']= ''
        item['groupe']= ''
        item['comp']= ''
        item['jour']= ''
        item['heure']= ''
        item['age']= ''
        item['europ']= ''
        item['amat']= ''
        item['lice']= ''
        item['temperature']= ''
        item['forceVent']= ''
        item['directionVent']= ''
        item['nebulositeLibelleCourt']= ''
        item['quinte']= False
        words = {
            'sex': ['Femelles','Mâles'],
            'handi': ['Handicap', 'handicap'],
            'meteo': ['Terrain', 'terrain'],
            'corde': ['Corde', 'corde'],
            'tempscourse': ['Temps total '],
            'lice': ['Lice'],
            'typec': ['Attelé','Haies','Monté','Steeple-chase'],
            'reclam': ['réclame'],
            'autos': ['autostart'],
            'natpis': ['piste en dirt','piste en dur','piste en herbe ','piste en sable','sable fibré'],
            'pistegp' : ['Petite piste', 'Grande piste', 'Moyenne piste'],
            'groupe' : ['Groupe'],
            'europ' : ['Course Européenne', 'Course Nationale'],
            'amat' : ['amateurs','Apprentis et Lads-jockeys','Gentlemen-riders et Cavalières','Jeunes Jockeys et Apprentis'],
        }
        wordsRegex = {
            'dist': r".*[0-9]{3,} m\xe8tres",
            'courseabc': r"Course [A-Z] ",
            'cheque': ur"[0-9]*[0-9]*[0-9]*.*[0-9]*[0-9]*[0-9]*.*[0-9][0-9][0-9] [£\u20AC\$RKYFDPZ]",
        }
        partant = 0

        item['url'] = response.meta['url']
        print('url========>', item['url'], item['prix'])

        comp = re.search('-[0-9]{6,}$', item['url'])
        try:
            comp = comp.group()
        except Exception as e:
            dataq_num = response.xpath("//li[contains(concat(' ', normalize-space(@class), ' '), ' current ')]/a/@href").extract()
            dataq_num = ''.join(dataq_num)
            title = ''.join(e)
            body = 'Pas d\'info pour cette course<br>line => comp = comp.group()<br><a href="http://www.paris-turf.com' + dataq_num + '">' + 'http://www.paris-turf.com' + dataq_num + '</a>'
            mailer.send(to=["mailto@gmail.com"], subject=title, body=body)
            return
        item['comp'] = comp.replace('-', '')

        item['jour'] = response.meta['jour']
        item['id'] = int(item['comp']) * int(item['jour'][0:4])

        arriv = response.xpath('//*[@class="CourseHeader-subTitle"]/strong/text()').extract()
        item['arriv'] = ''.join(arriv)

        prixnom = response.xpath('//*[@class="CourseHeader-title"]/strong/text()').extract()
        if prixnom == []:
            prixnom = response.xpath('//*[@class="text-regular text-normal text-size-md no-margin-bottom line-height-rg"]/strong/text()').extract()
        item['prixnom'] = ''.join(prixnom)

        hippo = response.xpath('//*[@id="main-page"]/div[2]/header/ol/li[3]/a/span/text()').extract()
        item['hippo'] = ''.join(hippo).replace('Réunion ', '')
        #Enlève Paris pour ParisLongchamp
        item['hippo'] = ''.join(item['hippo']).replace('Paris', '')

        reun = response.xpath('//*[@class="CourseHeader-title"]/text()').re(r'R[0-9]+')
        if reun != []:
            item['reun'] = ''.join(reun).replace('Réunion ', '')
            item['reun'] =  item['reun'].replace('R', '')
        else:
            reunion = response.xpath('//*[@class="text-regular text-normal text-size-md no-margin-bottom line-height-rg"]/text()').re(r'R[0-9]+')
            item['reun'] = ''.join(reunion).replace('Réunion ', '')
            item['reun'] =  item['reun'].replace('R', '')

        if item['reun'] == '':
            item['reun'] = item['hippo']

        condis = response.xpath('//*[@class="row-fluid row-no-margin text-left"]/p/text()[1]').extract()
        try:
            condi =  ''.join(condis[0]).replace('\n', '')
        except:
            condi = ''
        condi = condi.replace('  ', '')
        condi = condi.replace(' : ', '')
        item['condi'] = condi

        age = re.findall(r"Pour .*?[^d]ans.*?(?=, |\.)", condi, re.UNICODE)
        age1 = ''.join(age)
        age = re.findall(r"[0-9]+", age1, re.UNICODE)
        item['age'] = '-'.join(age)

        infos = response.xpath('//*[@class="row-fluid row-no-margin text-left"]/p/strong/text()').extract()

        for info in infos:
            infs = info.split(' - ')
            for cle, valeur in words.items():
                matching = [s for s in infs if any(xs in s for xs in valeur)]
                if matching != []:
                    if words[cle]: del words[cle]
                    item[cle] = ''.join(matching)

            for cle, valeur in wordsRegex.items():
                for inf in infs:
                    matching = re.findall(valeur, inf, re.UNICODE)
                    if matching != []:
                        if wordsRegex[cle]: del wordsRegex[cle]
                        item[cle] = ''.join(matching)


        if item['prix'] != '':
            item['prix'] = item['prix'].replace('C', '')
        if item['corde'] != '':
            item['corde'] = item['corde'].replace('Corde à ', '')
            item['corde'] = item['corde'][6:]
            # Si corde, alors course de plat
            if item['typec'] == '':
                item['typec'] = 'Plat'
        if item['dist'] != '':
            item['dist'] = item['dist'].replace(' mètres', '')
            item['dist'] = item['dist'].replace('.', '')
        if item['europ'] != '':
            item['europ'] = item['europ'].replace('Course Européenne', 'eur')
            item['europ'] = item['europ'].replace('Course Nationale', 'nat')
        if item['lice'] != '':
            item['lice'] = item['lice'].replace(' mètres', '')
            item['lice'] = item['lice'].replace(' mètre', '')
            item['lice'] = item['lice'].replace('Lice ', '')
        if item['tempscourse'] != '':
            item['tempscourse'] = item['tempscourse'].replace('Temps total ', '')

        my_file = Path('files/' + response.meta['jour'] + '/%s.json' % response.meta['jour'])

        try:
            os.makedirs('files/' + response.meta['jour'])
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        if my_file.is_file():
            print('***********=====files/' + response.meta['jour'] + '/%s.json' % response.meta['jour'])
            self.dataPmu = json.load(open('files/' + response.meta['jour'] + '/%s.json' % response.meta['jour']))
        else:
            r = requests.get('https://connect.awl.pmu.fr/turfInfo/client/1/programme/' + response.meta['jourdmY'])
            self.dataPmu = json.loads(r.text)
            # save le Json
            with io.open('files/' + response.meta['jour'] + '/%s.json' % response.meta['jour'], 'w', encoding='utf-8') as f:
                f.write(json.dumps(self.dataPmu, ensure_ascii=False))
                print('Saved !', response.meta['jour'] + '/%s.json' % response.meta['jour'])

        try:
            paris = timezone('Europe/Paris')
            timePmu = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['heureDepart']
            timePmu1 = datetime.datetime.utcfromtimestamp(int(str(timePmu)[:-3]))
            loc_dt = paris.localize(timePmu1)
            loc_dt += datetime.timedelta(hours=1)
            item['heure'] = loc_dt.strftime('%H:%M:%S')

            arrivee = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['statut']
            if (arrivee == "COURSE_ANNULEE"):
                item['arriv'] = 'Annulée'
        except:
            pass
       
        try:
            quinteArray = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['paris']
            for quinte in quinteArray:
                if quinte['codePari'] == 'QUINTE_PLUS':
                    item['quinte'] = True
                pass
        except:
            pass

        try:
            reun = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['numReunion']
            item['reun'] = reun
        except:
            pass

        try:
            sex = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['conditionSexe']
            sex = sex.replace('_', ' ').lower()
            item['sex'] = sex

        except:
            pass   


        try:
            item['temperature'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['temperature']
            item['forceVent'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['forceVent']
            item['directionVent'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['directionVent']
            item['nebulositeLibelleCourt'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['nebulositeLibelleCourt']
        except:
            pass


        # Cherche cl dans tableau arrivée
        tableau_partant = response.xpath('//table[@class="table table-striped sortable tooltip-enable arrivees"]/tbody').extract()
        tableau_partant = ''.join(tableau_partant)
        table_cl = []
        if (tableau_partant != ''):
            table_item = []
            for trtable in BeautifulSoup(tableau_partant, "lxml")("tr"):
                texttd = []
                for cell in trtable("td"):
                    if (cell.find('img') != None and cell.find('img').get('title') != None):
                        texttd.append(cell.find('img').get('title'))
                    else:
                        if (cell.text == ''):
                            texttd.append('-')
                        else:
                            texttd.append(cell.text)
                table_item.append(texttd)
            tableau_partant_cl = json.loads(json.dumps(table_item))
            for items in tableau_partant_cl:
                # Si 1er dai, 2eme dai... place le 'dai' avant le classement
                matching = re.findall(r'.+dai', items[0], re.UNICODE)
                if matching != []:
                    matching= ''.join(matching)
                    dai = matching.replace('dai', '')
                    items[0] = 'dai ' + dai

                table_cl.append(items[0])







        # Cherche les partants
        url = 'http://www.paris-turf.com/_js/course/tableaux/partants/' + response.meta['numcourse']
        return [Request(
            url=response.urljoin(url),
            meta={
                'jour': response.meta['jour'],
                'item': item,
                'table_cl': table_cl,
                'jourdmY': response.meta['jourdmY'],
                'numcourse': response.meta['numcourse']
            },
            callback=self.parse_race,
        )]

    # Scrape les chevaux partants
    def parse_race(self, response):
        print('=========================')
        print('======parse_race=========')
        print('=========================')
        jour = response.meta['jour']
        jourdmY = response.meta['jourdmY']
        table_cl = response.meta['table_cl']
        item = response.meta['item']
        numcourse = response.meta['numcourse']
        item['partant']= 0

        #PMU save json /participants si Réunion Premium
        self.dataPmuParticipant = ''
        if item['reun'] != []:
            p = requests.get('https://connect.awl.pmu.fr/turfInfo/client/1/programme/' + jourdmY + '/R' + str(item['reun']) + '/C' + str(item['prix']) + '/participants')
            self.dataPmuParticipant = json.loads(p.text)
            



        tableau_partant_th = response.xpath('//table[@class="table table-striped tooltip-enable table-row-hover sortable"]/thead').extract()
        tableau_partant_th = ''.join(tableau_partant_th)
        tableau_partant = response.xpath('//table[@class="table table-striped tooltip-enable table-row-hover sortable"]/tbody').extract()
        tableau_partant = ''.join(tableau_partant)

        comp = int(numcourse)
        numcourse = int(numcourse) * int(jour[0:4])

        tableau_partant_th_soup = [[cell.text for cell in row("th")]
            for row in BeautifulSoup(tableau_partant_th, "lxml")("tr")]
        tableau_partant_th_list = json.loads(json.dumps(tableau_partant_th_soup))
        if (tableau_partant_th_list ==[]):
            return
        tableau_partant_th_list = tableau_partant_th_list[0]
        tableau_partant_th_list = ['numero' if x== 'N°' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['cheval' if x== 'Cheval' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['ecar' if x== 'Ecart' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['dist' if x== 'Dist.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['corde' if x== 'Corde' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['txreclam' if x== 'TxRécl.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['entraineur' if x== 'Entraîneur' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['cotedirect' if x== 'Rapp. Ouv.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['coteprob' if x== 'Rapp.final PMU' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['redkm' if x== 'Réd. Km' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['tempstot' if x== 'Temps total' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['defoeil' if x== 'Oeill.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['defoeil' if x== 'Œill.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['defoeil' if x== 'Ferr.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['jockey' if x== 'Jockey' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['jockey' if x== 'Driver' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['poidmont' if x== 'Poids' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['musiquept' if x== 'Musique' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['vha' if x== 'V.Ha.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['recordG' if x== 'RecordGénéral' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['gains' if x== 'Gains' else x for x in tableau_partant_th_list]
        k = 0
        for valeur in tableau_partant_th_list:
            matching = re.findall(r'Rapp.PMU.*', valeur, re.UNICODE)
            if matching != []:
                tableau_partant_th_list[k] = 'cotedirect'
            matching = re.findall(r'Rapp. Prob.*', valeur, re.UNICODE)
            if matching != []:
                tableau_partant_th_list[k] = 'coteprob'
            matching = re.findall(r'Rapp. Ouv.*', valeur, re.UNICODE)
            if matching != []:
                tableau_partant_th_list[k] = 'coteprob'
            matching = re.findall(r'Rapp.LETURF.*', valeur, re.UNICODE)
            if matching != []:
                del tableau_partant_th_list[k]
            matching = re.findall(r'Rapp.GENYBET.*', valeur, re.UNICODE)
            if matching != []:
                del tableau_partant_th_list[k]
            k += 1

        # Sépare age et sex dans entetes
        pos_SA =  [i for i,x in enumerate(tableau_partant_th_list) if x == 'S/A']
        tableau_partant_th_list = ['age' if x== 'S/A' else x for x in tableau_partant_th_list]
        tableau_partant_th_list[pos_SA[0]:pos_SA[0]] = ['sexe']

        #check si distance présente, sinon ajoute distance
        pos_dist = [i for i,x in enumerate(tableau_partant_th_list) if x == 'dist']
        if pos_dist == []:
            tableau_partant_th_list.append('dist')

        # ajout colonne
        tableau_partant_th_list.append('cl')
        tableau_partant_th_list.append('jour')
        tableau_partant_th_list.append('id')
        tableau_partant_th_list.append('comp')
        tableau_partant_th_list.append('numcourse')
        tableau_partant_th_list.append('typec')
        tableau_partant_th_list.append('partant')
        tableau_partant_th_list.append('commen')
        tableau_partant_th_list.append('pere')
        tableau_partant_th_list.append('mere')
        tableau_partant_th_list.append('gainsCarriere')
        tableau_partant_th_list.append('gainsVictoires')
        tableau_partant_th_list.append('gainsPlace')
        tableau_partant_th_list.append('gainsAnneeEnCours')
        tableau_partant_th_list.append('gainsAnneePrecedente')
        tableau_partant_th_list.append('jumentPleine')
        tableau_partant_th_list.append('engagement')
        tableau_partant_th_list.append('proprietaire')
        tableau_partant_th_list.append('handicapDistance')
        tableau_partant_th_list.append('handicapPoids')
        tableau_partant_th_list.append('indicateurInedit')
        tableau_partant_th_list.append('ecurie')
        tableau_partant_th_list.append('tempstot')
        tableau_partant_th_list.append('redkm')
        item['th'] = tableau_partant_th_list

        table_item = []
        for trtable in BeautifulSoup(tableau_partant, "lxml")("tr"):
            texttd = []
            for cell in trtable("td"):
                if (cell.find('img') != None and cell.find('img').get('title') != None):
                    texttd.append(cell.find('img').get('title'))
                else:
                    if (cell.text == ''):
                        texttd.append('-')
                    else:
                        texttd.append(cell.text)
                if (cell.has_attr('colspan') and int(cell["colspan"]) == 3):
                    texttd.append('-')
                    texttd.append('-')
            texttd = [w.replace('Déferré des 4 pieds', 'D4') for w in texttd]
            texttd = [w.replace('Déferré des antérieurs', 'DA') for w in texttd]
            texttd = [w.replace('Déferré des postérieurs', 'DP') for w in texttd]
            texttd = [w.replace('Déferré pour la première fois des 4 pieds', 'D4/1') for w in texttd]
            texttd = [w.replace('Déferré pour la première fois des antérieurs', 'DA/1') for w in texttd]
            texttd = [w.replace('Déferré pour la première fois des postérieurs', 'DP/1') for w in texttd]
            texttd = [w.replace('Port des oeillères pour la première fois', 'Oeil/1') for w in texttd]
            texttd = [w.replace('Port des oeillères australiennes', 'Oeil/A') for w in texttd]
            texttd = [w.replace('Port des oeillères', 'Oeil') for w in texttd]
            texttd = [w.replace('NON PARTANTE', 'NP') for w in texttd]
            texttd = [w.replace('NON PARTANT', 'NP') for w in texttd]

            table_item.append(texttd)

        tableau_partant_list = json.loads(json.dumps(table_item))

        if (len(tableau_partant_list) > 1):
            nbPartant = len(tableau_partant_list)
            for items in tableau_partant_list:
                # Sépare age et sex dans tableau
                items[pos_SA[0]:pos_SA[0]] = [items[pos_SA[0]]]
                items[pos_SA[0]] =  ''.join(re.findall(r"[A-Z]", items[pos_SA[0]], re.UNICODE))
                items[pos_SA[0] + 1] =  ''.join(re.findall(r"[0-9]+", items[pos_SA[0] + 1], re.UNICODE))
                items.pop()

                # clean nom cheval et supprime Equipe E1, E2...
                items[2] = ' '.join(items[2].split())
                items[2] = re.sub('E[0-9]$', '', items[2])
                # print('cheval =============>', items[2])

                #Ajoute distance
                if pos_dist == []:
                    item['dist'] = item['dist'].replace('.', '')
                    items.append(item['dist'])

                # Ajoute classement dans tableau
                try:
                    items.append(table_cl[item['partant']])
                except:
                    items.append('')

                # Compte partant
                item['partant'] += 1
                # add jour, id, comp, numcourse, typec, partant
                items.append(jour)
                items.append(int(str(numcourse) + str(items[1])))
                items.append(comp)
                items.append(numcourse)
                items.append(item['typec'])
                items.append(nbPartant)

                try:
                    commenPmu = self.dataPmuParticipant['participants'][int(items[1]) - 1]['commentaireApresCourse']['texte']
                    items.append(commenPmu)
                except:
                    items.append('')
                try:
                    pere = self.dataPmuParticipant['participants'][int(items[1]) - 1]['nomPere']
                    items.append(pere)
                except:
                    items.append('')
                try:
                    mere = self.dataPmuParticipant['participants'][int(items[1]) - 1]['nomMere']
                    items.append(mere)
                except:
                    items.append('')
                try:
                    gainsCarriere = self.dataPmuParticipant['participants'][int(items[1]) - 1]['gainsParticipant']['gainsCarriere']
                    items.append(gainsCarriere)
                except:
                    items.append('')
                try:
                    gainsVictoires = self.dataPmuParticipant['participants'][int(items[1]) - 1]['gainsParticipant']['gainsVictoires']
                    items.append(gainsVictoires)
                except:
                    items.append('')
                try:
                    gainsPlace = self.dataPmuParticipant['participants'][int(items[1]) - 1]['gainsParticipant']['gainsPlace']
                    items.append(gainsPlace)
                except:
                    items.append('')
                try:
                    gainsAnneeEnCours = self.dataPmuParticipant['participants'][int(items[1]) - 1]['gainsParticipant']['gainsAnneeEnCours']
                    items.append(gainsAnneeEnCours)
                except:
                    items.append('')
                try:
                    gainsAnneePrecedente = self.dataPmuParticipant['participants'][int(items[1]) - 1]['gainsParticipant']['gainsAnneePrecedente']
                    items.append(gainsAnneePrecedente)
                except:
                    items.append('')
                try:
                    jumentPleine = self.dataPmuParticipant['participants'][int(items[1]) - 1]['jumentPleine']
                    items.append(jumentPleine)
                except:
                    items.append('')
                try:
                    engagement = self.dataPmuParticipant['participants'][int(items[1]) - 1]['engagement']
                    items.append(engagement)
                except:
                    items.append('')
                try:
                    proprietaire = self.dataPmuParticipant['participants'][int(items[1]) - 1]['proprietaire']
                    proprietaire = proprietaire.lower()
                    items.append(proprietaire)
                except:
                    items.append('')
                try:
                    handicapDistance = self.dataPmuParticipant['participants'][int(items[1]) - 1]['handicapDistance']
                    items.append(handicapDistance)
                except:
                    items.append('')
                try:
                    handicapPoids = self.dataPmuParticipant['participants'][int(items[1]) - 1]['handicapPoids']
                    items.append(handicapPoids)
                except:
                    items.append('')
                try:
                    indicateurInedit = self.dataPmuParticipant['participants'][int(items[1]) - 1]['indicateurInedit']
                    items.append(indicateurInedit)
                except:
                    items.append('')
                try:
                    ecurie = self.dataPmuParticipant['participants'][int(items[1]) - 1]['ecurie']
                    items.append(ecurie)
                except:
                    items.append('')
                try:
                    tempstot = self.dataPmuParticipant['participants'][int(items[1]) - 1]['tempsObtenu']
                    number_dec = str(tempstot)[3:5]
                    div = divmod(tempstot / 1000 , 60)
                    items.append(str(div[0])+ "'" +str(div[1]) + '"' + number_dec)
                except:
                    items.append('')
                try:
                    redkm = self.dataPmuParticipant['participants'][int(items[1]) - 1]['reductionKilometrique']
                    milli = datetime.datetime.fromtimestamp(redkm / 1000.0)
                    number_dec = int(milli.strftime('%f')) / 10000
                    div = divmod(redkm / 1000 , 60)
                    items.append(str(div[0])+ "'" +str(div[1]) + '"' + str(number_dec))
                except:
                    items.append('')

                try:
                    cotedirect = self.dataPmuParticipant['participants'][int(items[1]) - 1]['dernierRapportDirect']['rapport']
                    positionCote = item['th'].index('cotedirect')
                    items[positionCote] = cotedirect
                except:
                    pass

                try:
                    positionOeil = item['th'].index('defoeil')
                    if (items[positionOeil] == "-"):
                        defoeil = self.dataPmuParticipant['participants'][int(items[1]) - 1]['oeilleres']
                        defoeil = defoeil.replace('SANS_OEILLERES', '0')
                        defoeil = defoeil.replace('OEILLERES_AUSTRALIENNES', 'Oeil/A')
                        defoeil = defoeil.replace('OEILLERES_CLASSIQUE', 'Oeil')
                        items[positionOeil] = defoeil
                except:
                    positionOeil = item['th'].index('defoeil')
                    items[positionOeil] = "0"
                    pass

                for index, item1 in enumerate(items):
                    if (item1 == '-'):
                        items[index] = 0


            # supprime champ vide
            pos_empty =  [i for i,x in enumerate(tableau_partant_th_list) if x == '']
            del tableau_partant_th_list[pos_empty[0]]
            for items in tableau_partant_list:
                del items[pos_empty[0]]
            item['alll'] = json.loads(json.dumps(tableau_partant_list))
            return item
