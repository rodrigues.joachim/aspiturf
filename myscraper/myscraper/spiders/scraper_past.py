# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

#Cherche courses dans le passé
import datetime
import io
import json
import os
import os.path
import re
import sys
import time
from imp import reload
from pathlib import Path
from pprint import pprint
from random import randint

import pytz
import requests
import scrapy
from bs4 import BeautifulSoup
from pytz import timezone
from scrapy.http.response.html import HtmlResponse
from scrapy.linkextractors import LinkExtractor
from scrapy.mail import MailSender
from scrapy.spiders import CrawlSpider, Request, Rule

from myscraper.items import MyscraperItem

mailer = MailSender(smtphost='localhost', mailfrom='mailfrom@gmail.com', smtpuser='', smtppass='', smtpport=1025)

reload(sys)
sys.setdefaultencoding('utf-8')

class PTDirectorySpider(CrawlSpider):
    name = 'scraper_past'
    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 10,
        'CONCURRENT_REQUESTS': 10,
        'RETRY_TIMES': 0,
        'ITEM_PIPELINES': {
            'myscraper.pipelines.MyscraperPartant': 300,
            'myscraper.pipelines.MyscraperCaractRap': 500
        },

    }

    def start_requests(self):
        self.logger.info('tagdate==========> %s', self.tagdate)
        self.date = datetime.datetime.strptime(self.tagdate, '%Y-%m-%d')
        for x in xrange(1, int(self.i) + 1):
            # PMU
            self.datedmY = self.date.strftime('%d%m%Y')
            self.dateYmd = self.date.strftime('%Y-%m-%d')
            
            # Test si fichier Json déjà présent
            yield scrapy.Request(
                'http://127.0.0.1:8000/' + self.dateYmd,
                meta={
                    'jour': self.dateYmd,
                    'jourdmY': self.datedmY
                },
                callback=self.parse
            )
            self.date += datetime.timedelta(days=1)


    def parse(self, response):
        urls = response.xpath(u'//li/a/@href').extract()

        self.logger.info('urls ==========> %s', urls)
        for url in urls:
            # try:
            # Ne prend pas les .json
            if (url[-4:] != 'json'):
                yield Request(
                    url=response.urljoin(url),
                    callback=self.parse_race,
                    meta={
                        'jour': response.meta['jour'],
                        'jourdmY': response.meta['jourdmY']
                    },
                )
            
    def parse_race(self, response):
        item = MyscraperItem()

        item['arriv']= ''
        item['prixnom']= ''
        item['hippo']= ''
        item['reun']= ''
        item['prix']= ''
        item['sex']= ''
        item['handi']= ''
        item['meteo']= ''
        item['tempscourse']= ''
        item['corde']= ''
        item['dist']= ''
        item['typec']= ''
        item['reclam']= ''
        item['courseabc']= ''
        item['autos']= ''
        item['cheque']= ''
        item['natpis']= ''
        item['condi']= ''
        item['pistegp']= ''
        item['groupe']= ''
        item['comp']= ''
        item['jour']= ''
        item['heure']= ''
        item['partant']= ''
        item['age']= ''
        item['europ']= ''
        item['amat']= ''
        item['lice']= ''
        item['temperature']= ''
        item['forceVent']= ''
        item['directionVent']= ''
        item['nebulositeLibelleCourt']= ''
        item['quinte']= False
        words = {
            'sex': ['Femelles','Mâles'],
            'handi': ['Handicap', 'handicap'],
            'meteo': ['Terrain', 'terrain'],
            'tempscourse': ['Temps total '],
            'corde': ['Corde', 'corde'],
            'lice': ['Lice'],
            'typec': ['Attelé','Haies','Monté','Steeple-chase'],
            'reclam': ['réclame'],
            'autos': ['autostart'],
            'natpis': ['piste en dirt','piste en dur','piste en herbe ','piste en sable','sable fibré'],
            'pistegp' : ['Petite piste', 'Grande piste', 'Moyenne piste'],
            'groupe' : ['Groupe'],
            'europ' : ['Course Européenne', 'Course Nationale'],
            'amat' : ['amateurs','Apprentis et Lads-jockeys','Gentlemen-riders et Cavalières','Jeunes Jockeys et Apprentis'],
        }
        wordsRegex = {
            'dist': r".*[0-9]{3,} m\xe8tres",
            'courseabc': r"Course [A-Z] ",
            'cheque': ur"[0-9]*[0-9]*[0-9]*.*[0-9]*[0-9]*[0-9]*.*[0-9][0-9][0-9] [£\u20AC\$RKYFDPZ]",
        }
        partant = 0

        url = response.xpath('//a[contains(@title,"Voir le tableau des partants")]/@href').extract()
        item['url'] = ''.join(url)
        self.logger.info('item url==========> %s', item['url'])

        comp = re.search('-[0-9]{6,}$', item['url'])
        try:
            comp = comp.group()
        except Exception as e:
            dataq_num = response.xpath("//li[contains(concat(' ', normalize-space(@class), ' '), ' current ')]/a/@href").extract()
            dataq_num = ''.join(dataq_num)
            title = ''.join(e)
            body = 'Pas d\'info pour cette course<br>line => comp = comp.group()<br><a href="http://paris-turf.com' + dataq_num + '">' + 'http://paris-turf.com' + dataq_num + '</a>'
            mailer.send(to=["mailto@gmail.com"], subject=title, body=body)
            return
        item['comp'] = comp.replace('-', '')

        jour = re.findall(r"\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/", item['url'], re.UNICODE)
        item['jour'] = ''.join(jour)
        item['jour'] = item['jour'].replace('/', '')

        item['id'] = int(item['comp']) * int(item['jour'][0:4])

        for tr in response.xpath('//table[@class="table table-striped sortable tooltip-enable arrivees"]/tbody/tr'):
            partant += 1
        item['partant'] = partant

        arriv = response.xpath('//*[@class="CourseHeader-subTitle"]/strong/text()').extract()
        item['arriv'] = ''.join(arriv)

        prixnom = response.xpath('//*[@class="CourseHeader-title"]/strong/text()').extract()
        if prixnom == []:
            prixnom = response.xpath('//*[@class="text-regular text-normal text-size-md no-margin-bottom line-height-rg"]/strong/text()').extract()
        item['prixnom'] = ''.join(prixnom)
        hippo = response.xpath('//*[@id="main-page"]/div[2]/header/ol/li[3]/a/span/text()').extract()
        item['hippo'] = ''.join(hippo).replace('Réunion ', '')
        reun = response.xpath('//*[@class="CourseHeader-title"]/text()').re(r'R[0-9]+')
        if reun != []:
            reunion = response.xpath('//*[@class="CourseHeader-title"]/text()').re(r'R[0-9]+')
            item['reun'] = ''.join(reunion).replace('Réunion ', '')
            item['reun'] =  item['reun'].replace('R', '')
        else:
            item['reun'] = item['hippo']
        prix = response.xpath('//*[@class="CourseHeader-title"]/text()').re(r'C[0-9]+')
        self.logger.info('prix ==========> %s', prix)
        item['prix'] = ''.join(prix)


        condis = response.xpath('//*[@class="row-fluid row-no-margin text-left"]/p/text()[1]').extract()
        try:
            condi =  ''.join(condis[0]).replace('\n', '')
        except:
            condi = ''
        condi = condi.replace('  ', '')
        condi = condi.replace(' : ', '')
        item['condi'] = condi

        age = re.findall(r"Pour .*?[^d]ans.*?(?=, |\.)", condi, re.UNICODE)
        age1 = ''.join(age)
        age = re.findall(r"[0-9]+", age1, re.UNICODE)
        item['age'] = '-'.join(age)

        infos = response.xpath('//*[@class="row-fluid row-no-margin text-left"]/p/strong/text()').extract()

        for info in infos:
            infs = info.split(' - ')
            for cle, valeur in words.items():
                matching = [s for s in infs if any(xs in s for xs in valeur)]
                if matching != []:
                    if words[cle]: del words[cle]
                    item[cle] = ''.join(matching)

            for cle, valeur in wordsRegex.items():
                for inf in infs:
                    matching = re.findall(valeur, inf, re.UNICODE)
                    if matching != []:
                        if wordsRegex[cle]: del wordsRegex[cle]
                        item[cle] = ''.join(matching)


        if item['prix'] != '':
            item['prix'] = item['prix'].replace('C', '')
        if item['corde'] != '':
            item['corde'] = item['corde'].replace('Corde à ', '')
            item['corde'] = item['corde'][6:]
            # Si corde, alors course de plat
            if item['typec'] == '':
                item['typec'] = 'Plat'
        if item['dist'] != '':
            item['dist'] = item['dist'].replace(' mètres', '')
            item['dist'] = item['dist'].replace('.', '')
        if item['europ'] != '':
            item['europ'] = item['europ'].replace('Course Européenne', 'eur')
            item['europ'] = item['europ'].replace('Course Nationale', 'nat')
        if item['lice'] != '':
            item['lice'] = item['lice'].replace(' mètres', '')
            item['lice'] = item['lice'].replace(' mètre', '')
            item['lice'] = item['lice'].replace('Lice ', '')
        if item['tempscourse'] != '':
            item['tempscourse'] = item['tempscourse'].replace('Temps total ', '')

        my_file = Path('files/' + response.meta['jour'] + '/%s.json' % response.meta['jour'])
        if my_file.is_file():
            print('***********=====files/' + response.meta['jour'] + '/%s.json' % response.meta['jour'])
            self.dataPmu = json.load(open('files/' + response.meta['jour'] + '/%s.json' % response.meta['jour']))
        else:
            r = requests.get('https://connect.awl.pmu.fr/turfInfo/client/1/programme/' + response.meta['jourdmY'])
            self.dataPmu = json.loads(r.text)
            # save le Json
            with io.open('files/' + response.meta['jour'] + '/%s.json' % response.meta['jour'], 'w', encoding='utf-8') as f:
                f.write(json.dumps(self.dataPmu, ensure_ascii=False))
                print('Saved !', response.meta['jour'] + '/%s.json' % response.meta['jour'])

        

        try:
            paris = timezone('Europe/Paris')
            timePmu = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['heureDepart']
            timePmu1 = datetime.datetime.utcfromtimestamp(int(str(timePmu)[:-3]))
            loc_dt = paris.localize(timePmu1)
            loc_dt += datetime.timedelta(hours=1)
            item['heure'] = loc_dt.strftime('%H:%M:%S')
            print('hours', item['heure'])

            arrivee = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['statut']
            if (arrivee == "COURSE_ANNULEE"):
                item['arriv'] = 'Annulée'
        except:
            pass

        try:
            item['temperature'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['temperature']
            item['forceVent'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['forceVent']
            item['directionVent'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['directionVent']
            item['nebulositeLibelleCourt'] = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['meteo']['nebulositeLibelleCourt']
        except:
            pass

        try:
            reun = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['numReunion']
            item['reun'] = reun
        except:
            pass

        try:
            prix = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['numOrdre']
            item['prix'] = prix
        except:
            pass

        try:
            sex = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['conditionSexe']
            sex = sex.replace('_', ' ').lower()
            item['sex'] = sex

        except:
            pass

        try:
            quinteArray = self.dataPmu['programme']['reunions'][int(item['reun']) - 1]['courses'][int(item['prix']) - 1]['paris']
            for quinte in quinteArray:
                if quinte['codePari'] == 'QUINTE_PLUS':
                    item['quinte'] = True
                pass
        except:
            pass

        #PMU save json /participants si Réunion Premium
        jour = response.meta['jour']
        jourdmY = response.meta['jourdmY']
        self.dataPmuParticipant = ''
        if reun != []:
            my_file = Path('files/' + jour + '/%s.json' % item['comp'])
            if my_file.is_file():
                print('files/' + jour + '/%s.json' % item['comp'])
                self.dataPmuParticipant = json.load(open('files/' + jour + '/%s.json' % item['comp']))
            else:
                nbsecond = randint(0, 2)
                time.sleep(nbsecond)
                print(nbsecond, 'seconds')
                p = requests.get('https://connect.awl.pmu.fr/turfInfo/client/1/programme/' + jourdmY + '/R' + str(item['reun']) + '/C' + str(item['prix']) + '/participants')
                self.dataPmuParticipant = json.loads(p.text)
                # save le Json
                with io.open('files/' + jour + '/%s.json' % item['comp'], 'w', encoding='utf-8') as f:
                    f.write(json.dumps(self.dataPmuParticipant, ensure_ascii=False))
                    print('Saved !', jour + '/%s.json' % item['comp'])




        tableau_partant_th = response.xpath('//table[@class="table table-striped sortable tooltip-enable arrivees"]/thead').extract()
        tableau_partant_th = ''.join(tableau_partant_th)
        tableau_partant = response.xpath('//table[@class="table table-striped sortable tooltip-enable arrivees"]/tbody').extract()
        tableau_partant = ''.join(tableau_partant)

        url = response.xpath('//a[contains(@title,"Voir le tableau des partants")]/@href').extract()
        url = ''.join(url)
        item["url"] = url

        numcourse = re.search('-[0-9]{6,}$', url)
        try:
            numcourse = numcourse.group()
        except Exception as e:
            print('self ====================>', type(response))
            dataq_num = response.xpath("//li[contains(concat(' ', normalize-space(@class), ' '), ' current ')]/a/@href").extract()

            dataq_num = ''.join(dataq_num)
            title = ''.join(e)
            body = 'Pas d\'arrivée pour cette course<br>line => numcourse = numcourse.group()<br><a href="http://paris-turf.com' + dataq_num + '">' + 'http://paris-turf.com' + dataq_num + '</a>'
            mailer.send(to=["mailto@gmail.com"], subject=title, body=body)
            return
        numcourse = numcourse.replace('-', '')

        comp = int(numcourse)
        numcourse = int(numcourse) * int(jour[0:4])

        tableau_partant_th_soup = [[cell.text for cell in row("th")]
            for row in BeautifulSoup(tableau_partant_th, "lxml")("tr")]
        tableau_partant_th_list = json.loads(json.dumps(tableau_partant_th_soup))
        if (tableau_partant_th_list ==[]):
            return
        tableau_partant_th_list = tableau_partant_th_list[0]
        tableau_partant_th_list.remove('Rapp.final LETURF')
        tableau_partant_th_list.remove('Rapp.final GENYBET')
        tableau_partant_th_list = ['cl' if x== 'Cl.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['numero' if x== 'N°' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['cheval' if x== 'Cheval' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['ecar' if x== 'Ecart' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['dist' if x== 'Dist.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['corde' if x== 'Corde' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['txreclam' if x== 'TxRécl.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['entraineur' if x== 'Entraîneur' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['cotedirect' if x== 'Rapp. Ouv.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['coteprob' if x== 'Rapp.final PMU' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['redkm' if x== 'Réd. Km' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['tempstot' if x== 'Temps total' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['defoeil' if x== 'Oeill.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['defoeil' if x== 'Œill.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['defoeil' if x== 'Ferr.' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['jockey' if x== 'Jockey' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['jockey' if x== 'Driver' else x for x in tableau_partant_th_list]
        tableau_partant_th_list = ['poidmont' if x== 'Poids' else x for x in tableau_partant_th_list]

        # Sépare age et sex dans entes
        pos_SA =  [i for i,x in enumerate(tableau_partant_th_list) if x == 'S/A']
        tableau_partant_th_list = ['age' if x== 'S/A' else x for x in tableau_partant_th_list]
        tableau_partant_th_list[pos_SA[0]:pos_SA[0]] = ['sexe']

        #check si distance présente, sinon ajoute distance
        pos_dist = [i for i,x in enumerate(tableau_partant_th_list) if x == 'dist']
        if pos_dist == []:
            tableau_partant_th_list.append('dist')


        # ajout colonne
        tableau_partant_th_list.append('jour')
        tableau_partant_th_list.append('id')
        tableau_partant_th_list.append('comp')
        tableau_partant_th_list.append('numcourse')
        tableau_partant_th_list.append('typec')
        tableau_partant_th_list.append('partant')
        tableau_partant_th_list.append('commen')
        tableau_partant_th_list.append('pere')
        tableau_partant_th_list.append('mere')
        tableau_partant_th_list.append('gainsCarriere')
        tableau_partant_th_list.append('gainsVictoires')
        tableau_partant_th_list.append('gainsPlace')
        tableau_partant_th_list.append('gainsAnneeEnCours')
        tableau_partant_th_list.append('gainsAnneePrecedente')
        tableau_partant_th_list.append('jumentPleine')
        tableau_partant_th_list.append('engagement')
        tableau_partant_th_list.append('proprietaire')
        tableau_partant_th_list.append('handicapDistance')
        tableau_partant_th_list.append('handicapPoids')
        tableau_partant_th_list.append('indicateurInedit')
        tableau_partant_th_list.append('ecurie')
        tableau_partant_th_list.append('redkmInt')
        item['th'] = tableau_partant_th_list

        table_item = []
        for trtable in BeautifulSoup(tableau_partant, "lxml")("tr"):
            texttd = []
            for cell in trtable("td"):
                if (cell.find('img') != None and cell.find('img').get('title') != None):
                    texttd.append(cell.find('img').get('title'))
                else:
                    if (cell.text == ''):
                        texttd.append('-')
                    else:
                        texttd.append(cell.text)
            texttd = [w.replace('Déferré des 4 pieds', 'D4') for w in texttd]
            texttd = [w.replace('Déferré des antérieurs', 'DA') for w in texttd]
            texttd = [w.replace('Déferré des postérieurs', 'DP') for w in texttd]
            texttd = [w.replace('Déferré pour la première fois des 4 pieds', 'D4/1') for w in texttd]
            texttd = [w.replace('Déferré pour la première fois des antérieurs', 'DA/1') for w in texttd]
            texttd = [w.replace('Déferré pour la première fois des postérieurs', 'DP/1') for w in texttd]
            texttd = [w.replace('Port des oeillères', 'Oeil') for w in texttd]
            texttd = [w.replace('Port des oeillères australiennes', 'Oeil/A') for w in texttd]
            texttd = [w.replace('Oeil australiennes', 'Oeil/A') for w in texttd]
            texttd = [w.replace('Port des oeillères pour la première fois', 'Oeil/1') for w in texttd]
            texttd = [w.replace('Oeil pour la première fois', 'Oeil/1') for w in texttd]
            texttd = [w.replace('NON PARTANTE', 'NP') for w in texttd]
            texttd = [w.replace('NON PARTANT', 'NP') for w in texttd]
            # Si 1er dai, 2eme dai... place le 'dai' avant le classement
            matching = re.findall(r'.+dai', texttd[0], re.UNICODE)
            if matching != []:
                matching= ''.join(matching)
                dai = matching.replace('dai', '')
                texttd[0] = 'dai ' + dai

            table_item.append(texttd)


        tableau_partant_list = json.loads(json.dumps(table_item))

        if (len(tableau_partant_list) > 1):
            nbPartant = len(tableau_partant_list)
            for items in tableau_partant_list:
                # Sépare age et sex dans tableau
                items[pos_SA[0]:pos_SA[0]] = [items[pos_SA[0]]]
                items[pos_SA[0]] =  ''.join(re.findall(r"[A-Z]", items[pos_SA[0]], re.UNICODE))
                items[pos_SA[0] + 1] =  ''.join(re.findall(r"[0-9]+", items[pos_SA[0] + 1], re.UNICODE))
                items.pop()


                # clean nom cheval et supprime Equipe E1, E2...
                items[3] = ' '.join(items[3].split())
                items[3] = re.sub('E[0-9]$', '', items[3])

                #Ajoute distance
                if pos_dist == []:
                    item['dist'] = item['dist'].replace('.', '')
                    items.append(item['dist'])

                # add jour, id, compt, numcourse, typec, partant
                items.append(jour)
                items.append(int(str(numcourse) + str(items[2])))
                items.append(comp)
                items.append(numcourse)
                items.append(item['typec'])
                items.append(item['partant'])

                try:
                    commenPmu = self.dataPmuParticipant['participants'][int(items[2]) - 1]['commentaireApresCourse']['texte']
                    items.append(commenPmu)
                except:
                    items.append('')
                try:
                    pere = self.dataPmuParticipant['participants'][int(items[2]) - 1]['nomPere']
                    items.append(pere)
                except:
                    items.append('')
                try:
                    mere = self.dataPmuParticipant['participants'][int(items[2]) - 1]['nomMere']
                    items.append(mere)
                except:
                    items.append('')

                try:
                    gainsCarriere = self.dataPmuParticipant['participants'][int(items[2]) - 1]['gainsParticipant']['gainsCarriere']
                    items.append(gainsCarriere)
                except:
                    items.append('')
                try:
                    gainsVictoires = self.dataPmuParticipant['participants'][int(items[2]) - 1]['gainsParticipant']['gainsVictoires']
                    items.append(gainsVictoires)
                except:
                    items.append('')
                try:
                    gainsPlace = self.dataPmuParticipant['participants'][int(items[2]) - 1]['gainsParticipant']['gainsPlace']
                    items.append(gainsPlace)
                except:
                    items.append('')
                try:
                    gainsAnneeEnCours = self.dataPmuParticipant['participants'][int(items[2]) - 1]['gainsParticipant']['gainsAnneeEnCours']
                    items.append(gainsAnneeEnCours)
                except:
                    items.append('')
                try:
                    gainsAnneePrecedente = self.dataPmuParticipant['participants'][int(items[2]) - 1]['gainsParticipant']['gainsAnneePrecedente']
                    items.append(gainsAnneePrecedente)
                except:
                    items.append('')
                try:
                    jumentPleine = self.dataPmuParticipant['participants'][int(items[2]) - 1]['jumentPleine']
                    items.append(jumentPleine)
                except:
                    items.append('')
                try:
                    engagement = self.dataPmuParticipant['participants'][int(items[2]) - 1]['engagement']
                    items.append(engagement)
                except:
                    items.append('')
                try:
                    proprietaire = self.dataPmuParticipant['participants'][int(items[2]) - 1]['proprietaire']
                    proprietaire = proprietaire.lower()
                    items.append(proprietaire)
                except:
                    items.append('')
                try:
                    handicapDistance = self.dataPmuParticipant['participants'][int(items[2]) - 1]['handicapDistance']
                    items.append(handicapDistance)
                except:
                    items.append('')
                try:
                    handicapPoids = self.dataPmuParticipant['participants'][int(items[2]) - 1]['handicapPoids']
                    items.append(handicapPoids)
                except:
                    items.append('')
                try:
                    indicateurInedit = self.dataPmuParticipant['participants'][int(items[2]) - 1]['indicateurInedit']
                    items.append(indicateurInedit)
                except:
                    items.append('')
                try:
                    ecurie = self.dataPmuParticipant['participants'][int(items[2]) - 1]['ecurie']
                    items.append(ecurie)
                except:
                    items.append('')

                try:
                    redkmInt = self.dataPmuParticipant['participants'][int(items[2]) - 1]['reductionKilometrique']
                    items.append(redkmInt)
                except:
                    items.append('')

                try:
                    cotedirect = self.dataPmuParticipant['participants'][int(items[2]) - 1]['dernierRapportDirect']['rapport']
                    positionCote = item['th'].index('cotedirect')
                    items[positionCote] = cotedirect
                except:
                    pass

                for index, item1 in enumerate(items):
                    if (item1 == '-'):
                        items[index] = 0


        # supprime champ vide
        pos_empty =  [i for i,x in enumerate(tableau_partant_th_list) if x == '']
        del tableau_partant_th_list[pos_empty[0]]
        for items in tableau_partant_list:
            del items[pos_empty[0]]
        item['alll'] = json.loads(json.dumps(tableau_partant_list))

        return item
