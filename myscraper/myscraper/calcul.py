# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division

import datetime

import MySQLdb
from scrapy.utils.project import get_project_settings
from twisted.enterprise import adbapi

settings = get_project_settings()

class Calcul(object):
    # c.execute("""SELECT spam, eggs, sausage FROM breakfast WHERE price < %s""", (max_price,))

    def __init__(self):
        dbargs = settings.get('DB_CONNECT')
        db_server = settings.get('DB_SERVER')
        dbpool = adbapi.ConnectionPool(db_server, **dbargs)
        self.dbpool = dbpool
        self.conn = MySQLdb.connect(**dbargs)
        self.cursor = self.conn.cursor()

    def Recense(self, cheval, date):
        dateFormat = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        sql = """SELECT `jour` FROM `cachedate` WHERE `cheval` = %s AND `jour` < %s AND `cl` != 'NP' ORDER BY `cachedate`.`jour` DESC limit 1"""

        self.cursor.execute(sql, (cheval, date))
        m = self.cursor.fetchone()

        if (m != None):
            try:
                recense = dateFormat - m[0]
                return recense.days
            except:
                return 0
        return 0

    def MusiqueCheval(self, cheval, date):
        sql = """SELECT `jour`, `cl`, `cheval` FROM `cachedate` WHERE `cheval` = %s AND `jour` < %s ORDER BY `cachedate`.`jour` DESC """

        musiqueCheval = [0, 0, 0, 0, 0, 0]
        self.cursor.execute(sql, (cheval, date))
        musiques = self.cursor.fetchall()

        if (musiques != None):
            try:
                i = 0
                for musique in musiques:
                    if (musique[1] != None and i < 6 and musique[1] != 'NP'):
                        musiqueCheval[i] = musique[1]
                        musiqueCheval[i] = musiqueCheval[i].replace('er', '')
                        musiqueCheval[i] = musiqueCheval[i].replace('e', '')
                        musiqueCheval[i] = musiqueCheval[i].replace('dai', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('Npl.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('tbé', '10')
                        # musiqueCheval[i] = musiqueCheval[i].replace('NP', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dér.t.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('arr.', '10')
                        musiqueCheval[i] = int(musiqueCheval[i])
                        i += 1
            except Exception as e:
                return musiqueCheval
        return musiqueCheval

    def MusiqueHistory(self, titlename, name, date):
        dateFormat = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        dateFormatMinusYear = dateFormat - datetime.timedelta(days=365)
        dateFormatMinusYear = dateFormatMinusYear.strftime('%Y-%m-%d')

        sql = """SELECT `jour`, `cl`, `""" + titlename + """`, `typec` FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` < %s"""
        if (titlename != 'cheval'):
            sql += """   AND `jour` >= %s """
        # if (titlename != 'cheval'):
        #     sql += """ limit 30"""
        sql += """  AND `cl` != 'NP' ORDER BY `cachedate`.`jour` DESC """

        # [Nbcourses, musique, courses gagnées, courses placées]
        musiqueName = [0,u'',0,0]
        if (titlename != 'cheval'):
            self.cursor.execute(sql, (name, date, dateFormatMinusYear))
        else:
            self.cursor.execute(sql, (name, date))
        musiques = self.cursor.fetchall()
        musiqueName[0] = int(self.cursor.rowcount)

        yearPresent = str(dateFormat.year)[-2:]
        if (musiques != None):
            # try:
            countMusique = 0
            if (titlename == 'cheval'):
                maxMusique = 10000
            else:
                maxMusique = 100

            for musique in musiques:
                year = str(musique[0].year)[-2:]
                if (musique[1] != None and musique[1] != 'NP'):
                    if (countMusique < maxMusique):
                        if (year != yearPresent):
                            musiqueName[1] += '(' + year + ') '
                            yearPresent = year
                        musiqueName[1] += musique[1]
                        musiqueName[1] = musiqueName[1].replace('er', '')
                        musiqueName[1] = musiqueName[1].replace('e', '')
                        musiqueName[1] = musiqueName[1].replace('dai', 'D')
                        musiqueName[1] = musiqueName[1].replace('tbé', 'T')
                        musiqueName[1] = musiqueName[1].replace('dér.t.', 'Dt')
                        musiqueName[1] = musiqueName[1].replace('arr.', 'A')
                        try:
                            musiqueName[1] += str(musique[3])[0].lower() + ' '
                        except:
                            musiqueName[1] += ' '
                    if (musique[1] == '1er'):
                        musiqueName[2] += 1
                    if (musique[1] == '1er' or musique[1] == '2e' or musique[1] == '3e'):
                        musiqueName[3] += 1
                    countMusique += 1
            # except Exception as e:
                # return musiqueName
        return musiqueName

    def MonteJour(self, titlename, name, date):
        sql = """SELECT COUNT(id) as nb FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` = %s """
        return self.CleanResult(sql, name, date)

    def CouruJour(self, titlename, name, date):
        sql = """SELECT COUNT(id) as nb FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` = %s and `cl` != '' """
        return self.CleanResult(sql, name, date)

    def VictoireJour(self, titlename, name, date):
        sql = """SELECT COUNT(id) as nb FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` = %s and `cl` = '1er' """
        return self.CleanResult(sql, name, date)

    def CleanResult(self, sql, name, date):
        traiteVar = ''
        self.cursor.execute(sql, (name, date))
        traiteVar = self.cursor.fetchone()

        traiteVar = str(traiteVar[0])
        # print(name, 'traiteVar=============>', traiteVar)
        return traiteVar

    def Dernier(self, cheval, date):
        sql = """SELECT `numcourse`, `ch`.`jour`, `cl`, `cheval`, `jockey`, `entraineur`, `proprietaire`, `coteprob`, `cr`.`dist`, `cr`.`hippo`, `cr`.`partant`, `cr`.`cheque`, `ch`.`defoeil`  FROM `cachedate` AS ch LEFT JOIN caractrap AS cr on cr.id = ch.numcourse WHERE `cheval` = %s AND `ch`.`jour` < %s ORDER BY `ch`.`jour` DESC LIMIT 1"""
        self.cursor.execute(sql, (cheval, date))
        result = self.cursor.fetchone()
        if (result == None):
            result = [0,0,0,0,0,0,0,0,0,0,0,0,0]
        return result

        # `numcourse`, `ch`.`jour`, `cl`, `cheval`, `jockey`, `entraineur`, `proprietaire`, `coteprob`, `cr`.`dist`, `cr`.`hippo`, `cr`.`partant`, `cr`.`cheque`, , `cr`.`defoeil`

    def PourcVictHippo(self, titlename, name, hippo, date):
        results = 0
        resultGagnant = 0
        resultPlace = 0
        pourc = [0, 0, 0]
        sql = """SELECT `numcourse`, `jour`, `cl`, `cheval`, `jockey`, `entraineur`, `hippo` FROM `cachedate` WHERE `""" + titlename + """` = %s AND `hippo` = %s AND `jour` < %s  """
        self.cursor.execute(sql, (name, hippo, date))
        results = self.cursor.fetchall()
        nbTotal = int(self.cursor.rowcount)
        for result in results:
            if (result[2] == '1er'):
                # print('resultGagnant', results)
                # print('1er ===============>', result[2])
                resultGagnant += 1
            if (result[2] == '1er' or result[2] == '2e' or result[2] == '3e'):
                resultPlace += 1
        # pourc[0] => gagnant
        # pourc[1] => place
        # pourc[2] => nombre de course sur cet hippo
        try:
            pourc[0] = resultGagnant / nbTotal * 100
        except ZeroDivisionError:
            pourc[0] = 0
        try:
            pourc[1] = resultPlace / nbTotal * 100
        except ZeroDivisionError:
            pourc[1] = 0
        pourc[2] = nbTotal
        return pourc

    def couple(self, jockey, cheval, hippo, date):
        results = 0
        resultGagnant = 0
        resultPlace = 0
        pourc = [0, 0, 0, 0, 0]
        sql = """SELECT `numcourse`, `jour`, `cl`, `cheval`, `jockey` FROM `cachedate` WHERE `jockey` = %s AND `cheval` = %s AND `hippo` REGEXP %s AND `jour` < %s  """
        self.cursor.execute(sql, (jockey, cheval, hippo, date))
        results = self.cursor.fetchall()
        nbTotal = int(self.cursor.rowcount)
        for result in results:
            if (result[2] == '1er'):
                resultGagnant += 1
            if (result[2] == '1er' or result[2] == '2e' or result[2] == '3e'):
                    resultPlace += 1
        # pourc[0] => gagnant
        # pourc[1] => place
        # pourc[2] => nombre de course sur cet hippo
        # pourc[3] => %gagnant
        # pourc[4] => %place
        pourc[0] = resultGagnant
        pourc[1] = resultPlace
        pourc[2] = nbTotal
        try:
            pourc[3] = resultGagnant / nbTotal * 100
        except ZeroDivisionError:
            pourc[3] = 0
        try:
            pourc[4] = resultPlace / nbTotal * 100
        except ZeroDivisionError:
            pourc[4] = 0
        return pourc

    def RecordG(self, cheval, date):
        result = ''
        sql = """SELECT `redkm` FROM `cachedate` WHERE `cheval` = %s AND `redkm` != 0 AND `jour` < %s ORDER BY `cachedate`.`redkm` ASC limit 1  """
        self.cursor.execute(sql, (cheval, date,))
        result = self.cursor.fetchone()
        return result

    def Recul(self, date):
        # print('calcul Recul1=============', date)
        sql = """UPDATE cachedate AS t INNER JOIN (SELECT comp, min(dist) mdist FROM cachedate WHERE `jour` = %s GROUP BY comp) t1 ON t.comp = t1.comp AND t.dist > t1.mdist  SET recul = 1  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

    def CalculPourc(self, date):
        # print('calcul CalculPourc=============', date)
        sql = """UPDATE cachedate SET pourcVictJock = `victoiresjockey`*100/`coursesjockey` WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()
        sql = """UPDATE cachedate SET pourcPlaceJock = `placejockey`*100/`coursesjockey` WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        sql = """UPDATE cachedate SET pourcVictCheval = `victoirescheval`*100/`coursescheval` WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()
        sql = """UPDATE cachedate SET pourcPlaceCheval = `placescheval`*100/`coursescheval` WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        sql = """UPDATE cachedate SET pourcVictEnt = `victoiresentraineur`*100/`coursesentraineur` WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        sql = """UPDATE cachedate SET pourcPlaceEnt = `placeentraineur`*100/`coursesentraineur` WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        #Met à jour indicateurInedit si recence = 0
        sql = """UPDATE cachedate SET indicateurInedit = 1 WHERE `recence` = 0 AND `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()
