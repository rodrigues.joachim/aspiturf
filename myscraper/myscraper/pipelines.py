# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from twisted.enterprise import adbapi
from scrapy.utils.project import get_project_settings
import os, errno, re
import pdb
import MySQLdb
from scrapy.mail import MailSender
from calcul import Calcul


mailer = MailSender(smtphost='localhost', mailfrom='mailfrom@gmail.com', smtpuser='', smtppass='', smtpport=1025)
settings = get_project_settings()

#scraper_rapports
class MyscraperRapports(object):
    insert_sql = """replace into rapports (%s) values ( %s )"""
    def __init__(self):
        dbargs = settings.get('DB_CONNECT')

        self.conn = MySQLdb.connect(**dbargs)
        self.cursor = self.conn.cursor()

    def __del__(self):
        try:
            self.cursor.close()
            self.conn.close()
        except:
            pass

    def process_item(self, item, spider):
        self.insert_data(item, self.insert_sql)
        return item

    def insert_data(self, item, insert):
        keys = item['th']
        fields = u','.join(keys)
        qm = u','.join([u'%s'] * len(keys))
        sql = insert % (fields, qm)
        for itemsingle in item['alll']:
            data = []
            i = 0
            for k in keys:
                if (k != '' ):
                    data.append(itemsingle[i])
                i +=1
            self.cursor.execute(sql, data)
            self.conn.commit()

#scraper_futur
class MyscraperCaractRapFutur(object):
    insert_sql = """replace into caractrap (%s) values ( %s )"""

    def __init__(self):
        dbargs = settings.get('DB_CONNECT')
        db_server = settings.get('DB_SERVER')
        dbpool = adbapi.ConnectionPool(db_server, **dbargs)
        self.dbpool = dbpool

    def __del__(self):
        self.dbpool.close()

    def process_item(self, item, spider):
        self.insert_data(item, self.insert_sql)
        return item

    def insert_data(self, item, insert):
        keys = item.keys()
        fields = u','.join(keys)
        qm = u','.join([u'%s'] * len(keys))
        sql = insert % (fields, qm)
        data = [item[k] for k in keys]
        return self.dbpool.runOperation(sql, data)

#scraper_past
class MyscraperCaractRap(object):
    # Replace Table caractrap
    insert_sql = """replace into caractrap (%s) values ( %s )"""

    def __init__(self):
        dbargs = settings.get('DB_CONNECT')
        db_server = settings.get('DB_SERVER')
        dbpool = adbapi.ConnectionPool(db_server, **dbargs)
        self.dbpool = dbpool

    def __del__(self):
        try:
            self.dbpool.close()
        except:
            pass

    def process_item(self, item, spider):
        item.pop('alll', None)
        item.pop('th', None)
        self.insert_data(item, self.insert_sql)
        return item

    def insert_data(self, item, insert):
        keys = item.keys()
        fields = u','.join(keys)
        qm = u','.join([u'%s'] * len(keys))
        sql = insert % (fields, qm)
        data = [item[k] for k in keys]
        return self.dbpool.runOperation(sql, data)

#scraper_arriv
class MyscraperArriv(object):
    def __init__(self):
        print('UPDATE')
        dbargs = settings.get('DB_CONNECT')
        db_server = settings.get('DB_SERVER')
        dbpool = adbapi.ConnectionPool(db_server, **dbargs)
        self.dbpool = dbpool

        self.conn = MySQLdb.connect(**dbargs)
        self.cursor = self.conn.cursor()

    def __del__(self):
        self.dbpool.close()

    def process_item(self, item, spider):
        self.insert_data(item)
        return item

    def insert_data(self, item):
        keys = item['th']
        for itemsingle in item['alll']:
            data = []
            champ = []
            i = 0
            for k in keys:
                if k != 'id':
                    champ.append(k +' = %s,')
                else:
                    # determine id
                    id = itemsingle[i]
                    print('id', id)
                i +=1
            i = 0
            champ = ' '.join(champ)[:-1]

            sql = """update `cachedate` SET """ + champ + """ WHERE `cachedate`.`id` = %s """
            for k in keys:
                if (k != '' and k != 'id'):
                    data.append(itemsingle[i])
                i +=1
            data.append(id)
            self.cursor.execute(sql, data)

#scraper_partant
#scraper_past
class MyscraperPartant(object):
    insert_sql = """replace into cachedate (%s) values ( %s )"""
    calcul = Calcul()

    def __init__(self):
        dbargs = settings.get('DB_CONNECT')

        self.conn = MySQLdb.connect(**dbargs)
        self.cursor = self.conn.cursor()

    def __del__(self):
        try:
            self.cursor.close()
            self.conn.close()
        except:
            pass

    def process_item(self, item, spider):
        self.insert_data(item, self.insert_sql)
        return item

    def insert_data(self, item, insert):
        item['th'].extend(['recence', 'm1', 'm2', 'm3', 'm4', 'm5', 'm6', 'musiqueche', 'musiquejoc', 'musiqueent', 'montesdujockeyjour', 'couruejockeyjour', 'victoirejockeyjour', 'monteentraineurjour', 'courueentraineurjour', 'victoireentraineurjour', 'coursescheval', 'victoirescheval', 'placescheval', 'coursesentraineur', 'victoiresentraineur', 'placeentraineur', 'coursesjockey', 'victoiresjockey', 'placejockey', 'dernierhippo', 'dernierealloc', 'derniernbpartants', 'dernieredist', 'derniereplace', 'dernierecote', 'dernierJoc', 'dernierEnt', 'dernierProp', 'defoeilPrec', 'hippo', 'pourcVictEntHippo', 'pourcVictJockHippo', 'pourcPlaceEntHippo', 'pourcPlaceJockHippo', 'pourcVictChevalHippo', 'pourcPlaceChevalHippo', 'nbrCourseJockHippo', 'nbrCourseChevalHippo', 'nbrCourseEntHippo', 'cheque', 'nbCourseCouple', 'TxVictCouple', 'TxPlaceCouple', 'nbVictCouple', 'nbPlaceCouple', 'nbCourseCoupleHippo', 'TxVictCoupleHippo', 'TxPlaceCoupleHippo', 'nbVictCoupleHippo', 'nbPlaceCoupleHippo'])

        # Ajoute recorgG dans la liste s'il n'y ai pas déjà
        try:
            item['th'].index('recordG')
        except:
            item['th'].extend(['recordG'])

        print(item['th'])

        keys = item['th']
        fields = u','.join(keys)
        qm = u','.join([u'%s'] * len(keys))
        sql = insert % (fields, qm)

        for itemsingle in item['alll']:
            
            data = []
            i = 0
            for k in keys:
                #cherche date
                if (k == 'jour'):
                    date = itemsingle[i]
                #cherche cheval
                if (k == 'cheval'):
                    cheval = itemsingle[i]
                i += 1

            i = 0
            musiqueCheval = []
            for k in keys:
                if (k == 'cheval'):
                    recenseCheval = self.calcul.Recense(itemsingle[i], date)
                    musiqueCheval = self.calcul.MusiqueCheval(itemsingle[i], date)
                    MusiqueChevalHistory = self.calcul.MusiqueHistory('cheval', itemsingle[i], date)
                    dernier = self.calcul.Dernier(itemsingle[i], date)
                    setRecordG = self.calcul.RecordG(itemsingle[i], date)
                    pourcVictHippoCheval = self.calcul.PourcVictHippo('cheval', itemsingle[i], item['hippo'], date)
                if (k == 'jockey' and  itemsingle[i] != 0 and itemsingle[i] != 'NP'):
                    # ça arrive que jockey = 0, donc on passe...
                    musiqueJockey = self.calcul.MusiqueHistory('jockey', itemsingle[i], date)
                    monteJockey = self.calcul.MonteJour('jockey', itemsingle[i], date)
                    courueJockey = self.calcul.CouruJour('jockey', itemsingle[i], date)
                    victoireJockey = self.calcul.VictoireJour('jockey', itemsingle[i], date)
                    pourcVictHippoJockey = self.calcul.PourcVictHippo('jockey', itemsingle[i], item['hippo'], date)
                    couple = self.calcul.couple(itemsingle[i], cheval, '.*', date)
                    coupleHippo = self.calcul.couple(itemsingle[i], cheval, item['hippo'], date)
                if (k == 'jockey' and (itemsingle[i] == 0 or itemsingle[i] == 'NP')):
                    musiqueJockey = [0, 0, 0, 0]
                    monteJockey = 0
                    courueJockey = 0
                    victoireJockey = 0
                    pourcVictHippoJockey = [0, 0, 0]
                    couple = [0, 0, 0, 0, 0]
                    coupleHippo = [0, 0, 0, 0, 0]
                if (k == 'entraineur' and itemsingle[i] != 0):
                    # ça arrive que entraineur = 0, donc on passe...
                    musiqueEntraineur = self.calcul.MusiqueHistory('entraineur', itemsingle[i], date)
                    monteEntraineur = self.calcul.MonteJour('entraineur', itemsingle[i], date)
                    courueEntraineur = self.calcul.CouruJour('entraineur', itemsingle[i], date)
                    victoireEntraineur = self.calcul.VictoireJour('entraineur', itemsingle[i], date)
                    pourcVictHippoEntraineur = self.calcul.PourcVictHippo('entraineur', itemsingle[i], item['hippo'], date)
                if (k == 'entraineur' and itemsingle[i] == 0):
                    musiqueEntraineur = [0, 0, 0, 0]
                    monteEntraineur = 0
                    courueEntraineur = 0
                    victoireEntraineur = 0
                    pourcVictHippoEntraineur = [0, 0, 0]
                if (k == 'recence'):
                    data.append(recenseCheval)
                elif (k == 'm1'):
                    data.append(musiqueCheval[0])
                elif (k == 'm2'):
                    data.append(musiqueCheval[1])
                elif (k == 'm3'):
                    data.append(musiqueCheval[2])
                elif (k == 'm4'):
                    data.append(musiqueCheval[3])
                elif (k == 'm5'):
                    data.append(musiqueCheval[4])
                elif (k == 'm6'):
                    data.append(musiqueCheval[5])
                elif (k == 'musiqueche'):
                    data.append(MusiqueChevalHistory[1])
                elif (k == 'musiquejoc'):
                    data.append(musiqueJockey[1])
                elif (k == 'musiqueent'):
                    data.append(musiqueEntraineur[1])
                elif (k == 'montesdujockeyjour'):
                    data.append(monteJockey)
                elif (k == 'couruejockeyjour'):
                    data.append(courueJockey)
                elif (k == 'victoirejockeyjour'):
                    data.append(victoireJockey)
                elif (k == 'monteentraineurjour'):
                    data.append(monteEntraineur)
                elif (k == 'courueentraineurjour'):
                    data.append(courueEntraineur)
                elif (k == 'victoireentraineurjour'):
                    data.append(victoireEntraineur)
                elif (k == 'coursescheval'):
                    data.append(MusiqueChevalHistory[0])
                elif (k == 'victoirescheval'):
                    data.append(MusiqueChevalHistory[2])
                elif (k == 'placescheval'):
                    data.append(MusiqueChevalHistory[3])
                elif (k == 'coursesentraineur'):
                    data.append(musiqueEntraineur[0])
                elif (k == 'victoiresentraineur'):
                    data.append(musiqueEntraineur[2])
                elif (k == 'placeentraineur'):
                    data.append(musiqueEntraineur[3])
                elif (k == 'coursesjockey'):
                    data.append(musiqueJockey[0])
                elif (k == 'victoiresjockey'):
                    data.append(musiqueJockey[2])
                elif (k == 'placejockey'):
                    data.append(musiqueJockey[3])
                elif (k == 'dernierhippo'):
                    data.append(dernier[9])
                elif (k == 'dernierealloc'):
                    data.append(dernier[11])
                elif (k == 'derniernbpartants'):
                    data.append(dernier[10])
                elif (k == 'dernieredist'):
                    data.append(dernier[8])
                elif (k == 'derniereplace'):
                    data.append(dernier[2])
                elif (k == 'dernierecote'):
                    data.append(dernier[7])
                elif (k == 'dernierJoc'):
                    data.append(dernier[4])
                elif (k == 'dernierEnt'):
                    data.append(dernier[5])
                elif (k == 'dernierProp'):
                    data.append(dernier[6])
                elif (k == 'defoeilPrec'):
                    data.append(dernier[12])
                elif (k == 'hippo'):
                    data.append(item['hippo'])
                elif (k == 'pourcVictEntHippo'):
                    data.append(pourcVictHippoEntraineur[0])
                elif (k == 'pourcVictJockHippo'):
                    data.append(pourcVictHippoJockey[0])
                elif (k == 'pourcPlaceEntHippo'):
                    data.append(pourcVictHippoEntraineur[1])
                elif (k == 'pourcPlaceJockHippo'):
                    data.append(pourcVictHippoJockey[1])
                elif (k == 'pourcVictChevalHippo'):
                    data.append(pourcVictHippoCheval[0])
                elif (k == 'pourcPlaceChevalHippo'):
                    data.append(pourcVictHippoCheval[1])
                elif (k == 'nbrCourseJockHippo'):
                    data.append(pourcVictHippoJockey[2])
                elif (k == 'nbrCourseChevalHippo'):
                    data.append(pourcVictHippoCheval[2])
                elif (k == 'nbrCourseEntHippo'):
                    data.append(pourcVictHippoEntraineur[2])
                elif (k == 'cheque'):
                    data.append(item['cheque'])
                elif (k == 'nbCourseCouple'):
                    data.append(couple[2])
                elif (k == 'TxVictCouple'):
                    data.append(couple[3])
                elif (k == 'TxPlaceCouple'):
                    data.append(couple[4])
                elif (k == 'nbVictCouple'):
                    data.append(couple[0])
                elif (k == 'nbPlaceCouple'):
                    data.append(couple[1])

                elif (k == 'nbCourseCoupleHippo'):
                    data.append(coupleHippo[2])
                elif (k == 'TxVictCoupleHippo'):
                    data.append(coupleHippo[3])
                elif (k == 'TxPlaceCoupleHippo'):
                    data.append(coupleHippo[4])
                elif (k == 'nbVictCoupleHippo'):
                    data.append(coupleHippo[0])
                elif (k == 'nbPlaceCoupleHippo'):
                    data.append(coupleHippo[1])
                elif (k == 'recordG'):
                    data.append(setRecordG)
                elif k != '':
                    try:
                        data.append(itemsingle[i])
                    except:
                        pass
                i += 1
            self.cursor.execute(sql, data)

            self.conn.commit()
        
    


        self.calcul.Recul(date)
        self.calcul.CalculPourc(date)

class HtmlFilePipeline(object):
    def process_item(self, item, spider):
        try:
            os.makedirs('files/' + item['jour'])
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        if item['html'] != None:
            file_name = re.search('[0-9]{6,}', item['url'])
            file_name = file_name.group()
            with open('files/' + item['jour'] + '/%s.html' % file_name, 'w+b') as f:
                f.write(item['html'])