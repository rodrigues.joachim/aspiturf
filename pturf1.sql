-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 19 Mai 2019 à 11:46
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  5.6.31-6+ubuntu17.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pturf2`
--
CREATE DATABASE IF NOT EXISTS `pturf1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pturf1`;

--
-- Structure de la table `cachedate`
--

CREATE TABLE `cachedate` (
  `id` bigint(11) NOT NULL,
  `comp` int(11) NOT NULL,
  `jour` date NOT NULL,
  `hippo` tinytext NOT NULL,
  `numcourse` bigint(11) NOT NULL,
  `cl` tinytext NOT NULL,
  `dist` smallint(4) NOT NULL,
  `partant` int(11) NOT NULL,
  `typec` text NOT NULL,
  `cheque` text NOT NULL,
  `numero` decimal(2,0) NOT NULL,
  `cheval` text NOT NULL,
  `sexe` text NOT NULL,
  `age` int(11) NOT NULL,
  `cotedirect` decimal(5,2) NOT NULL,
  `coteprob` decimal(5,2) NOT NULL,
  `recence` int(11) NOT NULL,
  `ecurie` text NOT NULL,
  `distpoids` text NOT NULL,
  `ecar` text NOT NULL,
  `redkm` text NOT NULL,
  `redkmInt` int(11) NOT NULL,
  `handiecords` text NOT NULL,
  `corde` decimal(2,0) NOT NULL,
  `defoeil` text NOT NULL,
  `defoeilPrec` text NOT NULL,
  `recul` tinyint(1) DEFAULT '0',
  `gains` text NOT NULL,
  `musiquept` text NOT NULL,
  `musiqueche` text NOT NULL,
  `m1` int(11) DEFAULT NULL,
  `m2` int(11) DEFAULT NULL,
  `m3` int(11) DEFAULT NULL,
  `m4` int(11) DEFAULT NULL,
  `m5` int(11) DEFAULT NULL,
  `m6` int(11) DEFAULT NULL,
  `jockey` text NOT NULL,
  `musiquejoc` text NOT NULL,
  `montesdujockeyjour` text NOT NULL,
  `couruejockeyjour` text NOT NULL,
  `victoirejockeyjour` text NOT NULL,
  `entraineur` text NOT NULL,
  `musiqueent` text NOT NULL,
  `monteentraineurjour` int(11) NOT NULL,
  `courueentraineurjour` int(11) NOT NULL,
  `victoireentraineurjour` int(11) NOT NULL,
  `coursescheval` int(11) NOT NULL,
  `victoirescheval` int(11) NOT NULL,
  `placescheval` int(11) NOT NULL,
  `coursesentraineur` int(11) NOT NULL,
  `victoiresentraineur` int(11) NOT NULL,
  `placeentraineur` int(11) NOT NULL,
  `coursesjockey` int(11) NOT NULL,
  `victoiresjockey` int(11) NOT NULL,
  `placejockey` int(11) NOT NULL,
  `dernierhippo` text,
  `dernierealloc` text,
  `derniernbpartants` text,
  `dernieredist` text,
  `derniereplace` text,
  `dernierecote` text,
  `dernierJoc` text,
  `dernierEnt` text,
  `dernierProp` text,
  `proprietaire` text NOT NULL,
  `nbcoursepropjour` int(11) NOT NULL,
  `europ` text NOT NULL,
  `amat` text NOT NULL,
  `arrive` text NOT NULL,
  `txrecl` text NOT NULL,
  `pays` text NOT NULL,
  `meteo` text NOT NULL,
  `lice` text NOT NULL,
  `natpis` text NOT NULL,
  `pistegp` text NOT NULL,
  `prix` text NOT NULL,
  `poidmont` text NOT NULL,
  `pourcVictJock` decimal(5,2) NOT NULL,
  `pourcPlaceJock` decimal(5,2) NOT NULL,
  `pourcVictCheval` decimal(5,2) NOT NULL,
  `pourcPlaceCheval` decimal(5,2) NOT NULL,
  `pourcVictEnt` decimal(5,2) NOT NULL,
  `pourcPlaceEnt` decimal(5,2) NOT NULL,
  `pourcVictEntHippo` decimal(5,2) NOT NULL,
  `pourcVictJockHippo` decimal(5,2) NOT NULL,
  `pourcPlaceEntHippo` decimal(5,2) NOT NULL,
  `pourcPlaceJockHippo` decimal(5,2) NOT NULL,
  `pourcVictChevalHippo` decimal(5,2) NOT NULL,
  `pourcPlaceChevalHippo` decimal(5,2) NOT NULL,
  `nbrCourseJockHippo` int(11) NOT NULL,
  `nbrCourseEntHippo` int(11) NOT NULL,
  `nbrCourseChevalHippo` int(11) NOT NULL,
  `nbCourseCouple` int(11) NOT NULL,
  `nbVictCouple` int(11) NOT NULL,
  `nbPlaceCouple` int(11) NOT NULL,
  `TxVictCouple` decimal(5,2) NOT NULL,
  `TxPlaceCouple` decimal(5,2) NOT NULL,
  `nbCourseCoupleHippo` int(11) NOT NULL,
  `nbVictCoupleHippo` int(11) NOT NULL,
  `nbPlaceCoupleHippo` int(11) NOT NULL,
  `TxVictCoupleHippo` decimal(5,2) NOT NULL,
  `TxPlaceCoupleHippo` decimal(5,2) NOT NULL,
  `pere` text NOT NULL,
  `mere` text NOT NULL,
  `peremere` text NOT NULL,
  `coteleturf` text NOT NULL,
  `commen` text NOT NULL,
  `gainsCarriere` int(11) NOT NULL,
  `gainsVictoires` int(11) NOT NULL,
  `gainsPlace` int(11) NOT NULL,
  `gainsAnneeEnCours` int(11) NOT NULL,
  `gainsAnneePrecedente` int(11) NOT NULL,
  `jumentPleine` tinyint(1) NOT NULL,
  `engagement` tinyint(1) NOT NULL,
  `handicapDistance` int(11) NOT NULL,
  `handicapPoids` int(11) NOT NULL,
  `indicateurInedit` tinyint(1) NOT NULL,
  `tempstot` text NOT NULL,
  `vha` text NOT NULL,
  `recordG` text,
  `recordGint` int(11) NOT NULL,
  `txreclam` text NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rangTxVictJock` int(11) NOT NULL,
  `rangTxVictCheval` int(11) NOT NULL,
  `rangTxVictEnt` int(11) NOT NULL,
  `rangTxPlaceJock` int(11) NOT NULL,
  `rangTxPlaceCheval` int(11) NOT NULL,
  `rangTxPlaceEnt` int(11) NOT NULL,
  `rangRecordG` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `caractrap`
--

CREATE TABLE `caractrap` (
  `id` bigint(11) NOT NULL,
  `comp` int(11) NOT NULL,
  `jour` varchar(255) DEFAULT NULL,
  `heure` time DEFAULT NULL,
  `hippo` varchar(255) DEFAULT NULL,
  `reun` varchar(255) DEFAULT NULL,
  `prix` double DEFAULT NULL,
  `prixnom` varchar(255) DEFAULT NULL,
  `meteo` varchar(255) DEFAULT NULL,
  `typec` varchar(255) DEFAULT NULL,
  `partant` varchar(255) DEFAULT NULL,
  `handi` varchar(255) DEFAULT NULL,
  `reclam` varchar(255) DEFAULT NULL,
  `dist` varchar(255) DEFAULT NULL,
  `groupe` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `corde` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `autos` varchar(255) DEFAULT NULL,
  `cheque` varchar(255) DEFAULT NULL,
  `europ` varchar(255) DEFAULT NULL,
  `quinte` double DEFAULT NULL,
  `natpis` varchar(255) DEFAULT NULL,
  `amat` varchar(255) DEFAULT NULL,
  `courseabc` varchar(255) DEFAULT NULL,
  `pistegp` varchar(255) DEFAULT NULL,
  `arriv` varchar(255) DEFAULT NULL,
  `lice` varchar(255) DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `forceVent` int(11) DEFAULT NULL,
  `directionVent` varchar(6) NOT NULL,
  `nebulositeLibelleCourt` mediumtext NOT NULL,
  `condi` varchar(255) DEFAULT NULL,
  `url` mediumtext NOT NULL,
  `tempscourse` mediumtext NOT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rapports`
--

CREATE TABLE `rapports` (
  `id` int(11) NOT NULL,
  `caractrap_id` bigint(11) NOT NULL,
  `rapport_type` varchar(45) NOT NULL,
  `pari_type` varchar(45) NOT NULL,
  `horses` varchar(45) NOT NULL,
  `rapport` decimal(10,2) NOT NULL,
  `jour` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `cachedate`
--
ALTER TABLE `cachedate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numcourse` (`numcourse`),
  ADD KEY `jour` (`jour`),
  ADD KEY `chejou` (`cheval`(10),`jour`),
  ADD KEY `propjour` (`proprietaire`(10),`jour`),
  ADD KEY `jourhippojoc` (`jour`,`hippo`(10),`jockey`(10)),
  ADD KEY `jourhippoent` (`jour`,`hippo`(10),`entraineur`(10)),
  ADD KEY `jourhippoche` (`jour`,`hippo`(10),`cheval`(10)),
  ADD KEY `jocjou` (`jockey`(20),`jour`) USING BTREE,
  ADD KEY `entjou` (`entraineur`(20),`jour`) USING BTREE,
  ADD KEY `dist` (`dist`),
  ADD KEY `comp` (`comp`);

--
-- Index pour la table `caractrap`
--
ALTER TABLE `caractrap`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rapports`
--
ALTER TABLE `rapports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `caractrap_id` (`caractrap_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `cachedate`
--
ALTER TABLE `cachedate`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `caractrap`
--
ALTER TABLE `caractrap`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rapports`
--
ALTER TABLE `rapports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
