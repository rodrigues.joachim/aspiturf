# Aspiturf

Extraire les données de sites de turf afin de les utiliser dans une base de données

## Pré-requis

* Python = 2.7

* Dépendances

```sudo apt-get install python-pip python-dev libmysqlclient-dev```

```pip install pathlib pytz requests bs4 mysqlclient```

* [Scrapy] - Framework Python pour extraire les données d'un site web

Ubuntu 14.04 et +

```sudo apt-get install libxml2-dev libxslt1-dev zlib1g-dev libffi-dev libssl-dev```

```pip install scrapy```

* [Scrapoxy] - (non obligatoire mais recommandé) Utiliser un proxy pour éviter de se faire blacklister
* Base de données Mysql

Pour la base de données Mysql, il faut vider la variable ```sql_mode``` 

Dans PhpMyAdmin par exemple, aller dans le menu Variables et chercher sql_mode. cliquez sur Modifier et vider le contenu

## Configuration

* Créer la base de données en utilisant le fichier pturf1.sql
  * caractrap => contient une ligne par courses
  * cachedate => contient une ligne par cheval. La colonne numcourse correspond à la colonne id de la table caractrap
  * rapports => contient tous les rapports. Les courses sont identifiées par caractrap_id qui correspond à l'id de la table caractrap
* Renommer le fichier ```settings.example``` => ```settings.py```
* Configurer l'accès à la base de données dans ```settings.py```

```sh
DB_CONNECT = {
    'db': 'pturf1',
    'user': 'user',
    'passwd': 'user',
    'host': 'localhost',
    'charset': 'utf8',
    'use_unicode': True,
}
```

## Utilisation

### Scrapoxy

Il est conseillé d'utiliser [Scrapoxy] afin de ne pas se faire blacklister par les sites chez qui on va chercher les informations.

Vous devez avoir un fichier de configuration ```conf.json``` et lancer Scrapoxy avec

```scrapoxy start conf.json -d```



### Importer les données des courses passées.
Pour cela, on télécharge les fichiers html et json avec 

```scraper_html.py```

Dans ce fichier, il faut configurer le proxy dans les custom_settings

Remplacer PASSWORD par le mot de passe qui est configuré dans ```conf.json```

```sh

custom_settings = {
    ...
    'PROXY': 'http://127.0.0.1:8888/?noconnect',
    'API_SCRAPOXY': 'http://127.0.0.1:8889/api',
    'API_SCRAPOXY_PASSWORD': 'PASSWORD',
    ...
    'DOWNLOADER_MIDDLEWARES': {
        'scrapoxy.downloadmiddlewares.proxy.ProxyMiddleware': 100,
        'scrapoxy.downloadmiddlewares.wait.WaitMiddleware': 101,
        'scrapoxy.downloadmiddlewares.scale.ScaleMiddleware': 102,
        'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
        'scrapoxy.downloadmiddlewares.blacklist.BlacklistDownloaderMiddleware': 950,
    }
}
```

Si vous n'utilisez pas [Scrapoxy], il faut commenter ou supprimer les lignes ci-dessus.

Lancer le téléchargement de fichiers:

```scrapy crawl scraper_html -a tagdate='2019-05-01' -a i=31```

* tagdate => date de début
* i => nombre de journée à télécharger

Les fichiers seront dans le dossier ```/files```

### Traitement des fichiers téléchargés

Se rendre dans le dossier ```/files``` et démarrer le serveur de fichier Python

```python -m SimpleHTTPServer 8000```

Une fois le serveur démarré sur le port 8000, lancer ```scraper_past```

```scrapy crawl scraper_past -a tagdate='2019-05-01' -a i=31```

Ce fichier va insérer toutes les informations dans la base de données. 
Ici on n'a pas besoin d'utiliser Scrapoxy puisqu'on utilise les fichier en local

### Insérer dans la base de données les courses du jour ou futures

Démarrer Scrapoxy

```scrapoxy start conf.json -d```

Dans ```scraper_partants.py``` mettre le mot de passe de Scrapoxy

```'API_SCRAPOXY_PASSWORD': 'PASSWORD',```

Si vous n'utilisez pas Scrapoxy, commentez les lignes

```sh
...
'PROXY': 'http://127.0.0.1:8888/?noconnect',
'API_SCRAPOXY': 'http://127.0.0.1:8889/api',
'API_SCRAPOXY_PASSWORD': 'PASSWORD',
...
'DOWNLOADER_MIDDLEWARES': {
            'scrapoxy.downloadmiddlewares.proxy.ProxyMiddleware': 100,
            'scrapoxy.downloadmiddlewares.wait.WaitMiddleware': 101,
            'scrapoxy.downloadmiddlewares.scale.ScaleMiddleware': 102,
            'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
            'scrapoxy.downloadmiddlewares.blacklist.BlacklistDownloaderMiddleware': 950,
        }
```

Traite les courses du jour et du lendamain

```scrapy crawl scraper_partant```

Traite les courses à une date donnée

```scrapy crawl scraper_partant -a tagdate='2019-05-01' -a i=1```

Avec ```scraper_partant``` les courses sont directement insérées dans la base de données

### Insérer les rapports dans la base de données

Les rapports sont lu dans les fichiers téléchargés par ```scraper_html```
Donc il faut démarrer le serveur Python dans ```/files```

```python -m SimpleHTTPServer 8000```

Puis pour traiter les fichiers du mois d'avril 2019 par exemple

```scrapy crawl scraper_rapports -a tagdate='2019-04-01' -a i=30```


# Licence GPLv3

 > This file is part of Aspiturf.

 > Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

 > Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

 > You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

[Scrapy]: <https://scrapy.org/>
[Scrapoxy]: <https://scrapoxy.readthedocs.io/en/master/>